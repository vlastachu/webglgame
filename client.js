var joinSectionId;
var chatSectionId;
var usersSectionId;
var warningsId;
var messagesId;
var usersId;
var shadowboxId;
var joinFormId;
var messageFormId;
var joinFormId;
var messagesId;
var textId;
var users = [];
var ws;

function createWebSocket(path) {
    var host = window.location.hostname;
    if (host == '') host = 'localhost';
    var uri = 'ws://' + host + ':9160' + path;
    var Socket = "MozWebSocket" in window ? MozWebSocket : WebSocket;
    return new Socket(uri);
}


function refreshUsers() {
    usersId.innerHTML = '';
    for(i in users) {
        var li = document.createElement('li');
        li.innerHTML = users[i];
        usersId.appendChild(li);
    }
}

function onMessage(event) {
    var p = document.createElement('p');
    p.innerHTML = event.data;
    
    messagesId.appendChild(p);
    //TODO: animate message
    
    if(event.data.match(/^[^:]* joined/)) {
        var user = event.data.replace(/ .*/, '');
        users.push(user);
        refreshUsers();
    }

    if(event.data.match(/^[^:]* disconnected/)) {
        var user = event.data.replace(/ .*/, '');
        var idx = users.indexOf(user);
        users = users.slice(0, idx).concat(users.slice(idx + 1));
        refreshUsers();
    }
}

function onJoinFormSubmit(){
    warningsId.innerHTML = "";
    var user = document.getElementById('user').value;
    ws = createWebSocket('/');

    ws.onopen = function() {
        ws.send('Hi! I am ' + user);
    };

    ws.onmessage = function(event) {
        if(event.data.match('^Welcome! Users: ')) {
            /* Calculate the list of initial users */
            var str = event.data.replace(/^Welcome! Users: /, '');
            if(str != "") {
                users = str.split(", ");
                refreshUsers();
            }
            joinSectionId.style.display = "none";
            shadowboxId.style.display = "none";

            ws.onmessage = onMessage;
        } else {
            warningsId.innerHTML += event.data;
            ws.close();
        }
    };
    document.getElementById('join').innerHTML = "Connecting"; //TODO: make progress animation
    return false;
}

function onMessageFormSubmit(){
    var text = textId.value;
    ws.send(text);
    textId.value = '';
    return false;
}

window.onload = function(){
    webGLStart();
    joinSectionId = document.getElementById('join-section');
    chatSectionId = document.getElementById('chat-section');
    usersSectionId = document.getElementById('users-section');
    warningsId = document.getElementById('warnings');
    messagesId = document.getElementById('messages');
    usersId = document.getElementById('users');
    shadowboxId = document.getElementById('shadowbox');
    joinFormId = document.getElementById('join-form');
    messageFormId = document.getElementsByName('message-form');
    joinFormId = document.getElementsByName('join-form');
    messagesId = document.getElementById('messages');
    textId = document.getElementById('text');
}